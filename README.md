# OpenAI Telegram Bot

This is a repository for a chatbot built using OpenAI's chat completion API. It allows users to ask the bot questions and receive human-like responses from the AI model.

## Prerequisites

1. [Get OpenAI API key](https://platform.openai.com/account/api-keys).
2. [Get Telegram bot token](https://core.telegram.org/bots/tutorial#obtain-your-bot-token).
3. [Install Docker](https://docs.docker.com/engine/install/).

   Here is command for installation on Debian/Ubuntu via convenience script:

   ```bash
   sudo apt update && \
   curl -fsSL https://get.docker.com -o get-docker.sh && \
   /bin/bash ./get-docker.sh && \
   sudo apt install -y docker-compose
   ```

## How to run locally

1. Clone this repo onto your local machine:

   ```bash
   git clone https://gitlab.com/ilmenshik/bot-openai
   ```

2. Create `.env` file with the necessary variables:

   ```bash
   LOG_LEVEL="DEBUG"

   TELEGRAM_API_TOKEN="<your-token-here>"
   TELEGRAM_WHITELISTED_USERS="<telegram-@username-of-admins-comma-separated>"
   TELEGRAM_IN_GROUP_TAG="@gpt"

   OPENAI_API_KEY="<your-token-here>"
   OPENAI_DEFAULT_MODEL="gpt-3.5-turbo" # [How can I access gpt-4](https://help.openai.com/en/articles/7102672-how-can-i-access-gpt-4)
   OPENAI_DEFAULT_TEMPERATURE="1.0"
   OPENAI_DEFAULT_MAX_TOKENS="4097"
   OPENAI_DEFAULT_MAX_MESSAGES="6"
   OPENAI_DEFAULT_MIN_PROMPT="0"

   REDIS_HOST="bot-openai-redis"
   REDIS_PORT=6380
   REDIS_VERSION="7.0.11-alpine3.18"
   ```

3. Run the bot using the command:

   ```bash
   docker-compose \
     -f ./docker-compose.yml \
     -f ./docker-compose-local.yml \
     up
   ```

## How to deploy

1. Set up necessary secrets in your secrets settings of your repo:

   - `OPENAI_API_KEY`
   - `TELEGRAM_API_TOKEN`
   - `TELEGRAM_WHITELISTED_USERS`


3. Make changes if you want to.
4. Make sure you have compatible [GitLab Runner](https://docs.gitlab.com/runner/) for this repo.
5. Push new tag, workflow **Build and Push Docker Images** will be triggered automatically.
6. Run workflow **Deploy Docker Images** manually with chosen tag to deploy image built at previous step.

## How to delete webHook
If you got an error:
```
TelegramConflictError: Telegram server says Conflict: can't use getUpdates method while webhook is active; use deleteWebhook to delete the webhook first
```

Then just run:
```bash
curl -X POST https://api.telegram.org/bot<TOKEN>/deleteWebhook
```