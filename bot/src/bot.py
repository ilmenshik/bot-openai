import logging as log
from os import environ
from os.path import basename
from typing import List, Dict
import sys
import openai
import asyncio
import inspect
import time
from textwrap import dedent
import simplejson as json
import pkg_resources as packages
from environs import Env
from pathlib import Path
from base64 import b64decode

from redis.asyncio.client import Redis

from aiogram import Bot, Dispatcher, Router
from aiogram.filters.command import Command, CommandStart
from aiogram.filters.exception import ExceptionTypeFilter
from aiogram.fsm.context import FSMContext
from aiogram.fsm.strategy import FSMStrategy
from aiogram.fsm.storage.redis import RedisStorage
from aiogram.enums import BotCommandScopeType, InputMediaType
from aiogram.enums.chat_action import ChatAction
from aiogram.types import (
    Message,
    User,
    Chat,
    ErrorEvent,
    BotCommand,
    BotCommandScopeChat,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    CallbackQuery,
    ForceReply,
    ReplyKeyboardRemove,
    InputMediaPhoto,
    FSInputFile,
)

from filters import (
    MessageFilter,
    IsNotGroup,
    IsNotCommand,
    IsPrivatePromptMessage,
    IsGroupPromptMessage,
    AuthorizedOnly,
    WhitelistedUsers,
    AddedUsers,
    IsImageRequest,
    CallbackCommand,
)

from aliases import SendMessageDelegate
from states import States
from utils import strip_markdown, split_string_to_batches, escape_markdown, get_timestamp
from images import ImageResponse, ImagePrompt, IMAGE_SIZES

env = Env()
env.read_env(recurse=True)

LOG_LEVEL: int = env.log_level("LOG_LEVEL", default=log.INFO)

log.basicConfig(stream=sys.stdout, level=LOG_LEVEL)

# HACK: Suppress error caused by late initialization of `working_set`
working_set = [] if packages.working_set is None else packages.working_set

log.debug(f"Packages:\n{json.dumps([package.project_name for package in working_set], indent=True)}")
log.debug(f"Environment:\n{json.dumps(dict(environ.items()), indent=True)}")


IS_IN_DOCKER: bool = env.bool("IS_IN_DOCKER", default=False)
TELEGRAM_API_TOKEN: str = env.str("TELEGRAM_API_TOKEN")
TELEGRAM_WHITELISTED_USERS: List[str] = env.list("TELEGRAM_WHITELISTED_USERS", default='')
TELEGRAM_IN_GROUP_TAG: str = env.str("TELEGRAM_IN_GROUP_TAG")
OPENAI_API_KEY: str = env.str("OPENAI_API_KEY")
OPENAI_DEFAULT_MODEL: str = env.str("OPENAI_DEFAULT_MODEL", default="gpt-3.5-turbo")
OPENAI_USE_MODEL: str = ''
OPENAI_DEFAULT_SIZE: str = env.str("OPENAI_DEFAULT_SIZE", default="1024x1024")
OPENAI_DEFAULT_MAX_TOKENS_LIMIT: int = 4097
OPENAI_DEFAULT_TEMPERATURE: float = env.float("OPENAI_DEFAULT_TEMPERATURE", default=1.0)
OPENAI_DEFAULT_MAX_TOKENS: int = env.int("OPENAI_DEFAULT_MAX_TOKENS", default=OPENAI_DEFAULT_MAX_TOKENS_LIMIT)
OPENAI_DEFAULT_MAX_MESSAGES: int = env.int("OPENAI_DEFAULT_MAX_MESSAGES", default=6)
OPENAI_DEFAULT_MIN_PROMPT: int = env.int("OPENAI_DEFAULT_MIN_PROMPT", default=0)
OPENAI_IMAGE_LOCKS = set()
OPENAI_IMAGES_DIR = Path.cwd() / "data"
OPENAI_IMAGES_RESPONSES_DIR = Path.cwd() / "data" / "responces"
REDIS_HOST: str = env.str("REDIS_HOST", default="localhost") if IS_IN_DOCKER else "localhost"
REDIS_PORT: int = env.int("REDIS_PORT", default="6379")

env.seal()
log.debug(f"Environment parsed:\n{env.dump()}")

bot = Bot(token=TELEGRAM_API_TOKEN, parse_mode="Markdown")
log.info("Bot initialized")

redis = Redis(host=REDIS_HOST,port=REDIS_PORT)
log.info("Redis initialized")

dispatcher = Dispatcher(storage=RedisStorage(redis), fsm_strategy=FSMStrategy.USER_IN_CHAT)
log.info("Dispatcher initialized")

router = Router()
dispatcher.include_router(router)
log.info("Router included into dispatcher")

send_message_delegates: List[SendMessageDelegate] = [
    lambda bot, chat_id, text: bot.send_message(chat_id, text, parse_mode="Markdown"),
    lambda bot, chat_id, text: bot.send_message(chat_id, text),
    lambda bot, chat_id, text: bot.send_message(chat_id, text, parse_mode="HTML"),
    lambda bot, chat_id, text: bot.send_message(chat_id, strip_markdown(text)),
    lambda bot, chat_id, text: bot.send_message(chat_id, f"```\n{text}\n```"),
]


#
# Helpers
#
@router.errors(ExceptionTypeFilter(PermissionError))
async def access_error_handler(error_event: ErrorEvent) -> None:
    message : Message = error_event.update.message # type: ignore
    log.warning('User rejected (not in the whitelist): {usr}'.format(
        usr=message.from_user.username if message.from_user else 'Unknown',
    ))


def get_model() -> str:
    return OPENAI_USE_MODEL or OPENAI_DEFAULT_MODEL


def set_model(model: str) -> None:
    global OPENAI_USE_MODEL
    OPENAI_USE_MODEL = model


async def log_message(message: Message, state: FSMContext) -> None:
    if LOG_LEVEL > log.DEBUG:
        return

    current_state = await state.get_state()
    data = await state.get_data()

    model = get_model()
    temperature = float(data.get("temperature", OPENAI_DEFAULT_TEMPERATURE))
    max_tokens = int(data.get("max_tokens", OPENAI_DEFAULT_MAX_TOKENS))
    max_messages = int(data.get("max_messages", OPENAI_DEFAULT_MAX_MESSAGES))

    messages: List[Dict[str, str]] = data.get("saved_messages", [])

    user_id = from_user.id if (from_user := message.from_user) is not None else None

    message_log_data = {
        "from_user": {
            "id": user_id
        },
        "state": {
            "name": current_state
        },
        "model": model,
        "temperature": temperature,
        "max_tokens": max_tokens,
        "max_messages": max_messages,
        "messages": {
            "count": len(messages)
        },
    }

    log.debug(f"Message\n{json.dumps(message_log_data, indent=True)}")


#
# Commands
#
@router.message(
    CommandStart(),
    IsNotGroup()
)
async def start_handler(message: Message, state: FSMContext) -> None:
    await state.set_state(States.default)

    commands_non_authorized = [
        BotCommand(command="id", description="Get Telegram IDs"),
        BotCommand(command="start", description="Refresh commands"),
    ]

    commands_authorized = [
        BotCommand(command="image", description="Draw an image with DALL-E"),
        BotCommand(command="set_creativity", description="Set GPT creativity"),
        BotCommand(command="reset", description="Reset conversation"),
    ]

    commands_elevated = [
        BotCommand(command="set_model", description="Choose ChatGPT model"),
        BotCommand(command="set_min_prompt", description="Set minimum prompt length"),
        BotCommand(command="set_max_tokens", description="Set token limit"),
        BotCommand(command="set_max_messages", description="Set context limit"),
        BotCommand(command="grant", description="Grant access to someone"),
        BotCommand(command="revoke", description="Revoke access from someone"),
        BotCommand(command="users", description="Show user list who have access to the bot"),
    ]

    commands_all = commands_non_authorized

    try:
        await WhitelistedUsers(TELEGRAM_WHITELISTED_USERS).__call__(message)
        commands_all.extend(commands_authorized)
        commands_all.extend(commands_elevated)
    except PermissionError:
        try:
            await AddedUsers(redis).__call__(message)
            commands_all.extend(commands_authorized)
        except PermissionError:
            pass

    commands_list = "\n".join(
        [f" - /{escape_markdown(c.command)} - {c.description}" for c in commands_all]
    )

    await message.reply(
        text=f"Hi! Write your *prompt* or check out available *commands*:\n{commands_list}",
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )
    await bot.set_my_commands(
        commands=commands_all,
        scope=BotCommandScopeChat(
            type=BotCommandScopeType.CHAT,
            chat_id=message.chat.id
        )
    )


@router.message(Command("id"))
async def get_my_telegram_id_handler(message: Message, state: FSMContext) -> None:
    user : User = message.from_user # type: ignore
    chat : Chat = message.chat

    response = dedent(f"""
    **User:**
    ```
      id: {user.id}
      is_bot: {user.is_bot}
      username: {user.username}
      first_name: {user.first_name}
      last_name: {user.last_name}
      is_premium: {user.is_premium}
      language_code: {user.language_code}
    ```
    
    **Chat:**
    ```
      id: {chat.id}
      type: {chat.type}
      title: {chat.title}
    ```
    """)

    await message.reply(
        text=f"{response}",
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


@router.message(
    Command("set_model"),
    IsNotGroup(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def set_model_handler(message: Message, state: FSMContext) -> None:
    model = get_model()
    await state.set_state(States.model)

    openai.api_key = OPENAI_API_KEY
    models_all = openai.Model.list()["data"] # type: ignore
    models_chat_completion = [model for model in models_all if model.id.startswith("text-davinci") or model.id.startswith("gpt-")]

    await message.reply(
        text=f"Choose OpenAI model (current: {model})",
        reply_markup=InlineKeyboardMarkup(
            inline_keyboard=[[InlineKeyboardButton(text=model.id, callback_data=model.id)] for model in models_chat_completion]
        )
    )


@router.callback_query(States.model)
async def callback_query_model_handler(callback_query: CallbackQuery, state: FSMContext) -> None:
    set_model(callback_query.data)
    model = get_model()

    await state.set_state(States.default)
    await callback_query.answer(f"Chosen model: {model}")

    message = callback_query.message

    if message is not None:
        await bot.send_message(
            chat_id=message.chat.id,
            text=f"Model changed to {model}"
        )


@router.message(
    Command("set_creativity"),
    IsNotGroup(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
async def set_temperature_handler(message: Message, state: FSMContext) -> None:
    data = await state.get_data()
    temperature = float(data.get("temperature", OPENAI_DEFAULT_TEMPERATURE))
        
    await state.set_state(States.temperature)
    buttons: List[List[InlineKeyboardButton]] = []

    rows_amount = 5

    for row_index in range(rows_amount):
        buttons.append([])

    temperature_max = 2
    temperature_multiplier = 10
    range_offset = 1

    for temperature_index in range(range_offset, ((temperature_max * temperature_multiplier) + range_offset)):
        row_index = (temperature_index - range_offset) // rows_amount
        temperature_value = str(temperature_index / float(temperature_multiplier))

        buttons[row_index].append(
            InlineKeyboardButton(
                text=temperature_value,
                callback_data=temperature_value
            )
        )

    await message.reply(
        text=f"Choose creativity for responses (current: {temperature})",
        reply_markup=InlineKeyboardMarkup(inline_keyboard=buttons)
    )


@router.callback_query(States.temperature)
async def callback_query_temperature_handler(callback_query: CallbackQuery, state: FSMContext) -> None:
    temperature = callback_query.data

    await state.update_data(temperature=temperature)
    await state.set_state(States.default)

    await callback_query.answer(f"Chosen creativity: {temperature}")

    message = callback_query.message

    if message is not None:
        await bot.send_message(
            chat_id=message.chat.id,
            text=f"Creativity changed to {temperature}"
        )


@router.message(
    Command("set_max_tokens"),
    IsNotGroup(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def set_max_tokens_handler(message: Message, state: FSMContext) -> None:
    data = await state.get_data()
    max_tokens = str(data.get("max_tokens", OPENAI_DEFAULT_MAX_TOKENS))

    await state.set_state(States.max_tokens)

    await message.reply(
        text=f"Enter a number of token limit per response.\nCurrent: {max_tokens}\nMaximum: {OPENAI_DEFAULT_MAX_TOKENS_LIMIT}.",
        reply_markup=ForceReply(
            force_reply=True,
            input_field_placeholder=str(OPENAI_DEFAULT_MAX_TOKENS)
        )
    )


@router.message(
    States.max_tokens,
    IsNotCommand(),
    IsNotGroup()
)
async def reply_max_tokens_handler(message: Message, state: FSMContext) -> None:
    max_tokens = message.text

    if max_tokens is None \
       or not max_tokens.isdigit() \
       or int(max_tokens) > OPENAI_DEFAULT_MAX_TOKENS_LIMIT:
        await message.reply(
            text=f"Enter a correct number of token limit per response.\nMaximum: {OPENAI_DEFAULT_MAX_TOKENS_LIMIT}",
            reply_markup=ForceReply(
                force_reply=True,
                input_field_placeholder=str(OPENAI_DEFAULT_MAX_TOKENS)
            )
        )

        return

    if max_tokens.strip() == "0":
        max_tokens = str(OPENAI_DEFAULT_MAX_TOKENS)

    await state.update_data(max_tokens=max_tokens)
    await state.set_state(States.default)

    await message.reply(
        text=f"Token limit changed to {max_tokens}",
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


@router.message(
    Command("set_max_messages"),
    IsNotGroup(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def set_max_messages_handler(message: Message, state: FSMContext) -> None:
    data = await state.get_data()
    max_messages = str(data.get("max_messages", OPENAI_DEFAULT_MAX_MESSAGES))

    await state.set_state(States.max_messages)

    await message.reply(
        text=f"Enter a number of context limit. 0 is for unlimited.\nCurrent: {max_messages}",
        reply_markup=ForceReply(
            force_reply=True,
            input_field_placeholder=str(OPENAI_DEFAULT_MAX_MESSAGES)
        )
    )


@router.message(
    States.max_tokens,
    IsNotCommand(),
    IsNotGroup()
)
async def reply_max_messages_handler(message: Message, state: FSMContext) -> None:
    max_messages = message.text

    if max_messages is None or not max_messages.isdigit():
        await message.reply(
            text="Enter a correct number of context limit. 0 is for unlimited",
            reply_markup=ForceReply(
                force_reply=True,
                input_field_placeholder=str(OPENAI_DEFAULT_MAX_MESSAGES)
            )
        )

        return

    await state.update_data(max_messages=max_messages)
    await state.set_state(States.default)

    await message.reply(
        text=f"Context limit changed to {max_messages}",
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


@router.message(
    Command("set_min_prompt"),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def set_min_prompt_handler(message: Message, state: FSMContext) -> None:
    data = await state.get_data()
    min_prompt = str(data.get("min_prompt", OPENAI_DEFAULT_MIN_PROMPT))

    await state.set_state(States.min_prompt)

    await message.reply(
        text=f"Enter a number of minimum prompt length. 0 is for unlimited.\nCurrent: {min_prompt}",
        reply_markup=ForceReply(
            force_reply=True,
            input_field_placeholder=str(OPENAI_DEFAULT_MIN_PROMPT)
        )
    )


@router.message(
    States.min_prompt,
    IsNotCommand(),
    IsNotGroup()
)
async def reply_min_prompt_handler(message: Message, state: FSMContext) -> None:
    min_prompt = message.text

    if min_prompt is None or not min_prompt.isdigit():
        await message.reply(
            text="Enter a correct number of minimum prompt length. 0 is for unlimited",
            reply_markup=ForceReply(
                force_reply=True,
                input_field_placeholder=str(OPENAI_DEFAULT_MIN_PROMPT)
            )
        )

        return

    await state.update_data(min_prompt=min_prompt)
    await state.set_state(States.default)

    await message.reply(
        text=f"Minimum prompt changed to {min_prompt}",
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


#
# Access
#
async def grant_access(username: str | None) -> str:
    if not username:
        return f"No valid contact, username or link provided"

    username = username.lower()
    granted_to = escape_markdown("@{}".format(username))
    if await user_granted(username):
        return f"User {granted_to} already in access list"
    
    rpush_task = redis.rpush("users", username)
    if inspect.isawaitable(rpush_task):
        await rpush_task
    else:
        return "Failed to grant access"

    return f"Access granted to user {granted_to}"


async def revoke_access(username: str | None) -> str:
    if not username:
        return f"No valid contact, username or link provided"

    username = username.lower()
    revoked_from = "@{}".format(username.replace('_', '\\_'))
    if not await user_granted(username):
        return f"User {revoked_from} not in access list"
    
    lrem_task = redis.lrem("users", 0, username)
    if inspect.isawaitable(lrem_task):
        await lrem_task
    else:
        return "Failed to revoke access"

    return f"Access revoked from user {revoked_from}"


async def user_granted(username_or_id: str) -> bool:
    ids = await get_granted_users()
    if not ids:
        return False
    
    return any([idx == username_or_id for idx in ids])


async def get_granted_users() -> List[str] | None:
    lrange_task = redis.lrange("users", 0, -1)
    if not inspect.isawaitable(lrange_task):
        return

    return sorted([x.decode() for x in await lrange_task])


@router.message(
    Command("grant"),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def add_user_handler(message: Message, state: FSMContext) -> None:
    if MessageFilter.is_group(message) < 0:
        if message.reply_to_message and message.reply_to_message.from_user:
            response = await grant_access(message.reply_to_message.from_user.username)
            await message.reply(text=response, reply_markup=ReplyKeyboardRemove(remove_keyboard=True))
    else:
        await state.set_state(States.add_user)
        await message.reply(
            text="Reply with username of user you want to grant access",
            reply_markup=ForceReply(force_reply=True)
        )


@router.message(
    States.add_user,
    IsNotCommand()
)
async def reply_add_user_handler(message: Message, state: FSMContext) -> None:
    if MessageFilter.is_group(message) or message.text is None:
        return
    
    username = basename(message.text.replace('@', '')) if message.text else None
    response = await grant_access(username)
    await state.set_state(States.default)
    await message.reply(text=response, reply_markup=ReplyKeyboardRemove(remove_keyboard=True))


@router.message(
    Command("users"),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def list_users_handler(message: Message, state: FSMContext) -> None:
    if MessageFilter.is_group(message):
        return
    
    await state.set_state(States.default)

    ids: List[str] | None = await get_granted_users()
    if ids is None:
        await message.reply(
            text="Failed to read list of users",
            reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
        )
        return

    user_list = ''
    for idx in ids:
        user_list += "- @{}\n".format(idx.replace('_', '\\_'))

    if not user_list:
        await message.reply(
            text=f"Nobody have access, except whiltelist: {', '.join(TELEGRAM_WHITELISTED_USERS)}",
            reply_markup=ReplyKeyboardRemove(remove_keyboard=True),
        )
    else:
        await message.reply(
            text=f"Access granted for:\n{user_list}",
            reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
        )


@router.message(
    Command("revoke"),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS)
)
async def remove_user_handler(message: Message, state: FSMContext) -> None:
    if MessageFilter.is_group(message) or message.text is None:
        reply = message.reply_to_message
        if not reply or not reply.from_user:
            return
        
        response = await revoke_access(reply.from_user.username)
        await message.reply(text=response, reply_markup=ReplyKeyboardRemove(remove_keyboard=True))
    else:
        await state.set_state(States.remove_user)
        await message.reply(
            text="Reply with username of user you want revoke access to this bot",
            reply_markup=ForceReply(force_reply=True)
        )


@router.message(
    States.remove_user,
    IsNotCommand()
)
async def reply_remove_user_handler(message: Message, state: FSMContext) -> None:
    if MessageFilter.is_group(message) or message.text is None:
        return
    
    username = basename(message.text.replace('@', '')) if message.text else None
    response = await revoke_access(username)
    await state.set_state(States.default)
    await message.reply(
        text=response,
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


#
# Images
#
@router.message(
    Command("image"),
    IsNotGroup(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
async def image_help_handler(message: Message, state: FSMContext) -> None:
    image_help = "Write a prompt with hashtag #image to draw an image."
    image_help += "\nUse #N to draw more variations (N=number)"
    image_help += "\nUse one of available sizes:"
    for size in IMAGE_SIZES:
        image_help += f"\n- {size.tag}, {size.size}, {size.price}, max variations: {size.max_variations}"

    await message.reply(
        text=image_help,
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


@router.message(
    IsImageRequest(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
async def image_handler(message: Message, state: FSMContext) -> None:
    await log_message(message, state)

    send_image_upload_task = asyncio.create_task(send_image_upload(message))
    send_answer_task = asyncio.create_task(send_image_prompt(message, state))

    concurrent_tasks = [send_image_upload_task, send_answer_task]

    done_tasks, _ = await asyncio.wait(concurrent_tasks, return_when=asyncio.FIRST_COMPLETED)

    undone_tasks = [concurrent_task for concurrent_task in concurrent_tasks if concurrent_task not in done_tasks]

    for undone_task in undone_tasks:
        undone_task.cancel()


@router.callback_query(
    CallbackCommand('image_retry'),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
async def callback_query_image_retry_handler(callback_query: CallbackQuery, state: FSMContext) -> None:
    if not callback_query.message \
            or not callback_query.message.reply_to_message \
            or not callback_query.message.reply_to_message.text:
        await callback_query.answer(f"Nothing to retry")
        return

    if callback_query.from_user.id in OPENAI_IMAGE_LOCKS:
        log.error("Ignored. Pleas wait")
        return

    await callback_query.answer(f"Trying to create new variation...")
    await image_handler(callback_query.message.reply_to_message, state)


async def send_image_upload(message: Message) -> None:
    try:
        while True:
            await bot.send_chat_action(
                chat_id=message.chat.id,
                action=ChatAction.UPLOAD_PHOTO
            )

            await asyncio.sleep(5)
    except asyncio.CancelledError:
        pass


async def send_image_prompt(message: Message, state: FSMContext) -> None:
    global OPENAI_IMAGE_LOCKS
    prompt = message.text.replace('#image', '').strip() if message.text else None
    if prompt is None or len(prompt) == 0:
        return

    prompt = prompt.replace(TELEGRAM_IN_GROUP_TAG, '').strip()
    if message.from_user.id in OPENAI_IMAGE_LOCKS:
        log.error(f"Prompt '{prompt}' ignored. Already in progress")
        return

    if OPENAI_DEFAULT_MIN_PROMPT and len(prompt) < OPENAI_DEFAULT_MIN_PROMPT:
        await message.reply(
            text="Sorry, the message is too short to process.",
            reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
        )
        return

    try:
        OPENAI_IMAGE_LOCKS.add(message.from_user.id)
        file_prefix = f"{message.from_user.username}_{message.chat.id}_{message.message_id}"
        response: ImageResponse = await get_image(ImagePrompt(message.text), file_prefix, message)
        if not response:
            await message.reply("No answer for that")
            return

        buttons: List[List[InlineKeyboardButton]] = [
            [
                InlineKeyboardButton(text="Retry", callback_data="image_retry")
            ]
        ]
        keyboard = InlineKeyboardMarkup(inline_keyboard=buttons)

        if not response.success:
            await message.reply(
                text=escape_markdown(response.message),
                reply_markup=keyboard
            )
            return

        if not response.images:
            await message.reply("Sorry. Something went wrong. Images not found.")
            return

        if len(response.images) == 1:
            photo = FSInputFile(response.images[0])
            await message.reply_photo(photo=photo, reply_markup=keyboard)
        else:
            media = [InputMediaPhoto(type=InputMediaType.PHOTO, media=FSInputFile(image)) for image in response.images]
            await message.reply_media_group(media)

    except BaseException as exception:
        log.error(exception)
        text = f"Exception occurred:\n```log\n{escape_markdown(str(exception))}\n```"
        await bot.send_message(message.chat.id, text)

    finally:
        OPENAI_IMAGE_LOCKS.remove(message.from_user.id)


async def get_image(prompt: ImagePrompt, file_prefix: str, message: Message) -> ImageResponse:
    openai.api_key = OPENAI_API_KEY

    while True:
        try:
            response = await openai.Image.acreate(
                model=prompt.size.model,
                prompt=prompt.prompt,
                n=prompt.variations,
                size=prompt.size.size or OPENAI_DEFAULT_SIZE,
                response_format="b64_json",
            )

            file_name = OPENAI_IMAGES_DIR / f"{file_prefix}_{response['created']}"
            result = []

            for index, image_dict in enumerate(response["data"]):
                image_data = b64decode(image_dict["b64_json"])
                image_file = OPENAI_IMAGES_DIR / f"{file_name}_{index}.png"
                result.append(image_file.as_posix())
                with open(image_file, mode="wb") as png:
                    png.write(image_data)

            return ImageResponse(images=result)

        except openai.error.InvalidRequestError as request_exception:
            return ImageResponse(success=False, message=str(request_exception))

        except openai.error.RateLimitError:
            return ImageResponse(
                success=False,
                message=str('Rate limit for images per minute (50) exceeded. Please wait and repeat your request later.')
            )

        except openai.error.OpenAIError as error:
            return ImageResponse(
                success=False,
                message=str(error.user_message) if MessageFilter.is_admin(message, TELEGRAM_WHITELISTED_USERS) else None
            )

        except BaseException as answer_exception:
            log.exception(f"Image answer exception: {answer_exception}")
            return ImageResponse(success=False)
#
# Answers
#
@router.message(
    IsGroupPromptMessage(bot, TELEGRAM_IN_GROUP_TAG),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
@router.message(
    IsPrivatePromptMessage(),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
async def prompt_handler(message: Message, state: FSMContext) -> None:
    await log_message(message, state)

    send_typing_task = asyncio.create_task(send_typing(message))
    send_answer_task = asyncio.create_task(send_prompt(message, state))

    concurrent_tasks = [send_typing_task, send_answer_task]

    done_tasks, _ = await asyncio.wait(concurrent_tasks, return_when=asyncio.FIRST_COMPLETED)

    undone_tasks = [concurrent_task for concurrent_task in concurrent_tasks if concurrent_task not in done_tasks]

    for undone_task in undone_tasks:
        undone_task.cancel()


async def send_message_with_retry(bot: Bot, chat_id: int, text: str) -> None:
    last_exception: BaseException | None = None

    for send_message_delegate in send_message_delegates:
        try:
            await send_message_delegate(bot, chat_id, text)
            return
        except BaseException as send_exception:
            log.error(f"Cannot parse answer:\n{text}")

            if "parse" not in str(send_exception):
                raise

            last_exception = send_exception

    raise Exception("Totally unknown exception") if last_exception is None else last_exception


async def send_prompt(message: Message, state: FSMContext) -> None:
    prompt = message.text
    if prompt is None or len(prompt) == 0:
        return

    prompt = prompt.replace(TELEGRAM_IN_GROUP_TAG, '').strip()
    data = await state.get_data()
    saved_messages: List[Dict[str, str]] = data.get("saved_messages", [])

    if OPENAI_DEFAULT_MIN_PROMPT and not saved_messages and len(prompt) < OPENAI_DEFAULT_MIN_PROMPT:
        await message.reply(
            text="Sorry, the message is too short to process.",
            reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
        )
        
        return

    saved_messages.append({"role": "user", "content": prompt})
    await state.update_data(saved_messages=saved_messages)

    try:
        answer = await get_answer(prompt, state)
        saved_messages.append({"role": "assistant", "content": answer})
        max_messages = int(data.get("max_messages", OPENAI_DEFAULT_MAX_MESSAGES))

        if max_messages != 0 and len(saved_messages) > max_messages:
            saved_messages.pop(0)

        await state.update_data(saved_messages=saved_messages)

        tag = ''
        if MessageFilter.is_group(message):
            tag = f"{TELEGRAM_IN_GROUP_TAG}\n"

        if len(answer.encode()) + len(tag) < 4096:
            await send_message_with_retry(bot, message.chat.id, tag + answer)
        else:
            for batch in split_string_to_batches(answer, 2048):
                await send_message_with_retry(bot, message.chat.id, tag + batch)

    except BaseException as exception:
        log.error(exception)
        text = f"Exception occurred:\n```log\n{exception}\n```"
        await bot.send_message(message.chat.id, text)


async def send_typing(message: Message) -> None:
    try:
        while True:
            await bot.send_chat_action(
                chat_id=message.chat.id,
                action=ChatAction.TYPING
            )

            await asyncio.sleep(5)
    except asyncio.CancelledError:
        pass


async def get_answer(prompt: str, state: FSMContext) -> str:
    openai.api_key = OPENAI_API_KEY

    data = await state.get_data()

    model = get_model()

    temperature = float(data.get("temperature", OPENAI_DEFAULT_TEMPERATURE))
    max_tokens = int(data.get("max_tokens", OPENAI_DEFAULT_MAX_TOKENS))
    max_messages = int(data.get("max_messages", OPENAI_DEFAULT_MAX_MESSAGES))

    messages: List[Dict[str, str]] = data.get("saved_messages", [{"role": "user", "content": prompt}])

    while True:
        try:
            response = await openai.ChatCompletion.acreate(
                model=model,
                temperature=temperature,
                max_tokens=None if max_tokens == OPENAI_DEFAULT_MAX_TOKENS_LIMIT else max_tokens,
                messages=messages
            )

            return response.choices[0].message.content # type: ignore

        except BaseException as answer_exception:
            if "maximum context length" in str(answer_exception):
                if len(messages) > 1:
                    messages.pop(0)
                else:
                    raise
            # Prevent flood:
            elif "tokens per min" in str(answer_exception):
                if len(messages) > max_messages:
                    while len(messages) > max_messages:
                        messages.pop(0)

                time.sleep(60)
            else:
                raise
        finally:
            await state.update_data(saved_messages=messages)


#
# Reset
#
@router.message(
    Command("reset"),
    AuthorizedOnly(TELEGRAM_WHITELISTED_USERS, redis)
)
async def reset_conversation_handler(message: Message, state: FSMContext) -> None:
    await state.set_state(States.default)
    await state.update_data(saved_messages=[])
    await message.reply(
        text=f"Conversation forgotten",
        reply_markup=ReplyKeyboardRemove(remove_keyboard=True)
    )


#
# Main
#
if __name__ == '__main__':
    log.info("Creating image dir")
    OPENAI_IMAGES_DIR.mkdir(exist_ok=True)
    log.info("About to run polling")
    dispatcher.run_polling(bot)
