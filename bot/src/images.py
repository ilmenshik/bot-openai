from re import compile
from typing import List


class ImageSize:
    def __init__(self, tag: str, model: str, size: str, price: str, max_variations: int = 1):
        self.model = model
        self.tag = tag
        self.size = size
        self.price = price
        self.max_variations = max_variations or 1


class ImageResponse:
    def __init__(self, images: List[str] | None = None, success: bool = False, message: str | None = None):
        self.images = images
        self.success = images and any(images)
        self.message = message or "Reason unknown"


class ImagePrompt:
    prompt: str
    size: ImageSize | None
    variations: int

    def __init__(self, message_text: str):
        self.prompt = message_text.replace('#image', '')
        self.size = None
        self.variations = 1

        self.parse_sizes()
        self.parse_variations()

        self.prompt = RE_STRIP_PROMPT_SPACES.sub(' ', self.prompt).strip()

    def parse_sizes(self) -> None:
        default_size = None
        for size in IMAGE_SIZES:
            if size.tag == 'default':
                default_size = size
                continue

            if size.tag in self.prompt:
                print("Found size:", size.tag)
                self.prompt = self.prompt.replace(size.tag, '')
                self.size = size
                return

        self.size = default_size

    def parse_variations(self) -> None:
        found_variations = RE_IMAGE_VARIATIONS_NUMBER.findall(self.prompt)
        if not found_variations:
            return

        variations = int(found_variations[0])
        if variations > OPENAI_MAX_IMAGE_VARIATIONS:
            variations = OPENAI_MAX_IMAGE_VARIATIONS

        if self.size and variations > self.size.max_variations:
            variations = self.size.max_variations

        self.variations = variations
        self.prompt = RE_IMAGE_VARIATIONS_NUMBER.sub('', self.prompt)


RE_STRIP_PROMPT_SPACES = compile(r' +')
OPENAI_MAX_IMAGE_VARIATIONS = 10
RE_IMAGE_VARIATIONS_NUMBER = compile(r'#(\d+)')
IMAGE_SIZES = [
    ImageSize('default', 'dall-e-2', '256x256', '$0.016', 10),
    ImageSize('#medium', 'dall-e-2', '512x512', '$0.018', 10),
    ImageSize('#large', 'dall-e-2', '1024x1024', '$0.020', 7),
    ImageSize("#quality", "dall-e-3", '1024x1024', '$0.040', 5),
]
