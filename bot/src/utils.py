from re import compile
from datetime import datetime

RE_TIMESTAMP_CLEAR = compile(r'[T:.]')


def get_timestamp():
    return RE_TIMESTAMP_CLEAR.sub('-', datetime.now().isoformat())


def strip_markdown(text: str) -> str:
    text = text.replace("**", "")
    text = text.replace("__", "")
    text = text.replace("~~", "")
    text = text.replace("`", "")
    text = text.replace("`", "")

    return text


def split_string_to_batches(string, max_length):
    return [string[i: i + max_length] for i in range(0, len(string), max_length)]


def escape_markdown(text: str) -> str:
    text = text.replace("_", "\\_")
    text = text.replace("*", "\\*")
    text = text.replace("`", "\\`")
    text = text.replace("~", "\\~")

    return text
