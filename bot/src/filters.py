import inspect
from re import compile

from typing import List

from redis.asyncio.client import Redis

from aiogram import Bot
from aiogram.filters import Filter
from aiogram.types import (
    Message,
    User,
    CallbackQuery,
)


class MessageFilter:
    @staticmethod
    def is_group(message: Message) -> bool:
        return message.chat.id < 0
    
    @staticmethod
    def is_private(message: Message) -> bool:
        return message.chat.id > 0

    @staticmethod
    def is_reply(message: Message) -> bool:
        return message.reply_to_message is not None
    
    @staticmethod
    def is_command(message: Message) -> bool:
        if message.text is None:
            return False

        return message.text.strip().startswith("/")

    @staticmethod
    def is_image_request(message: Message) -> bool:
        if message.text is None:
            return False

        return '#image' in message.text

    @staticmethod
    def is_admin(message: Message, ids: List[str]) -> bool:
        if message.from_user is None:
            return False

        user: User = message.from_user

        is_valid = user.username and any([idx == f"@{user.username}" for idx in ids])

        if not is_valid:
            raise PermissionError()

        return is_valid


class WhitelistedUsers(Filter):
    def __init__(self, ids: List[str]) -> None:
        self.ids: List[str] = ids

    async def __call__(self, message: Message) -> bool:
        return MessageFilter.is_admin(message, self.ids)


class AddedUsers(Filter):
    def __init__(self, redis: Redis) -> None:
        self.redis: Redis = redis

    async def __call__(self, message: Message) -> bool:
        if message.from_user is None:
            raise PermissionError("Failed to detect you")

        user: User = message.from_user

        lrange_task = self.redis.lrange("users", 0, -1)

        if not inspect.isawaitable(lrange_task):
            raise PermissionError("Failed to read list of users")

        ids: List[bytes] = await lrange_task
        is_valid = any([idx.decode() == user.username.lower() for idx in ids])

        if not is_valid:
            raise PermissionError()

        return is_valid


class AuthorizedOnly(Filter):
    def __init__(self, whitelisted_ids: List[str], redis: Redis | None = None) -> None:
        self.redis: Redis | None = redis
        self.whitelisted_ids: List[str] = whitelisted_ids

    async def __call__(self, message: Message) -> bool:
        try:
            if self.redis:
                return await AddedUsers(self.redis).__call__(message)
            else:
                return await WhitelistedUsers(self.whitelisted_ids).__call__(message)
        except PermissionError:
            return await WhitelistedUsers(self.whitelisted_ids).__call__(message)


class IsNotCommand(Filter):
    async def __call__(self, message: Message) -> bool:
        if message.text is None:
            return False

        return not message.text.startswith("/")


class IsNotGroup(Filter):
    async def __call__(self, message: Message) -> bool:
        return message.chat.id > 0


class CallbackCommand(Filter):
    def __init__(self, command: str):
        self.command = command

    async def __call__(self, data: CallbackQuery) -> bool:
        return data.data == self.command


class IsGroupPromptMessage(Filter):
    def __init__(self, bot: Bot, bot_tag: str):
        self.bot = bot
        self.reg_tagged = compile(r"^{tag}|{tag}$".format(tag=bot_tag))

    def is_tagged(self, message: Message) -> bool:
        return bool(message.text and self.reg_tagged.match(message.text.strip()))
    
    async def __call__(self, message: Message) -> bool:
        if not MessageFilter.is_group(message) or MessageFilter.is_image_request(message):
            return False

        if message.reply_to_message is None:
            return self.is_tagged(message)
        else:
            reply = message.reply_to_message
            if not reply.from_user or not reply.text:
                return False

            return bool(reply.from_user.id == self.bot.id and self.is_tagged(reply))


class IsPrivatePromptMessage(Filter):
    async def __call__(self, message: Message) -> bool:
        if MessageFilter.is_group(message):
            return False

        # None of below:
        return not any([
            MessageFilter.is_group(message),
            MessageFilter.is_reply(message),
            MessageFilter.is_command(message),
            MessageFilter.is_image_request(message),
        ])


class IsImageRequest(Filter):
    async def __call__(self, message: Message) -> bool:
        if message.text is None:
            return False

        return MessageFilter.is_image_request(message)
